const _=require('lodash');
const Inputs=require('./collections/inputs');
const Input=require('./models/input');
const Enumerable=require('./models/enumerable');
const TextInput=require('./models/text-input');
const FormPage=require('./models/form-page');
const Pages=require('./collections/pages');

function getPagesFromData(data){
  return new Pages(
    data.map(page=>new FormPage({
      title:page.title,
      slug:page.slug,
      inputs:getModelsFromData(page.inputs),
    }))
  );
}

function getModelsFromData(inputs){
  return new Inputs(
    inputs
      .map(input=>getModelFromData(input))
      .filter(input=>input!=null)
  );
}

function getModelFromData(input){
  const inputTypes={
    'enumerable':{
      model:Enumerable,
      options:(input)=>({
        multiple:input.allow_multiple_value,
        data:Object.keys(input.values),
      }),
    },
    'big_text':{
      model:TextInput,
      options:{
        textarea:true,
      },
    },
    'small_text':{
      model:TextInput,
      options:{
        textType:'text',
      },
    },
    'lat_lng':{
      model:TextInput,
      options:{
        textType:'text',
      },
    },
    'phone':{
      model:TextInput,
      options:{
        textType:'phone',
      },
    },
    'email':{
      model:TextInput,
      options:{
        textType:'email',
      },
    },
  };

  let result = inputTypes[input.type];
  if(_.isUndefined(result)) return null;

  return new result.model(
    _.assign(
      { 
        label:input.label, 
        name:input.name,
        placeholder:input.placeholder,
        required:input.required,
      }, 
      _.isUndefined(result.options)?{}:(
        _.isFunction(result.options)?result.options(input):result.options
      )
    )
  );
}

module.exports={getModelFromData,getModelsFromData,getPagesFromData};
