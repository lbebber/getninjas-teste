const Backbone=require('backbone');
const data=require('./data');
const _=require('lodash');

const Form=require('./models/form');
const FormView=require('./views/form');

const pagesData=[
  {
    title:'Seu pedido',
    slug:'seu-pedido',
    inputs:data._embedded.request_fields,
  },
  {
    title:'Seus dados',
    slug:'seus-dados',
    inputs:data._embedded.user_fields,
  }
];

const Routes=Backbone.Router.extend({
  routes:{
  },
});

const routes=new Routes();
Backbone.history.start()

const form=new Form({
  pages:pagesData,
  router:routes,
});

new FormView({
  model:form,
  el:document.querySelector('.Form'),
});
