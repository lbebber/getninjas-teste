const Backbone=require('backbone');

const TabsView=Backbone.View.extend({
  render(){
    this.el.innerHTML=this.model.get('pages')
      .map(page=>page.get('title'))
      .join('')
    ;
    return this;
  }
});

module.exports=TabsView;
