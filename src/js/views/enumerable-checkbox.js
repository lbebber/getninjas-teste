const Backbone=require('backbone');
const _=require('lodash');
const EnumerableView=require('./enumerable');

const EnumerableCheckboxView=EnumerableView.extend({
  changeValue(){
    let checkboxes=_.toArray(this.el.querySelectorAll('input[type="checkbox"]'));
    let value=checkboxes
      .filter(checkbox=>checkbox.checked)
      .map(checkbox=>checkbox.value);
    this.model.set('value',value);
    this.model.set('showRequiredWarning',false);
  },
  renderInput(){
    let required=this.model.get('required');

    let items=this.model.get('data')
      .map((item,i)=>{
        let id=this.model.get('name')+'-'+i;

        return `
          <li class='EnumerableCheckbox-item'>
            <input 
              type="checkbox" 
              ${required?'required':''}
              class="EnumerableCheckbox-input" 
              id="${id}" 
              name="${this.model.get('name')}" 
              value="${item}"
            />

            <label 
              for="${id}" 
              class="EnumerableCheckbox-label"
            >
              ${item}
            </label>
          </li>
        `;
      })
      .join('')
    ;

    return `
      <ol class='EnumerableCheckbox-list'>
        ${items}
      </ol>
    `;
  }
});

module.exports=EnumerableCheckboxView;
