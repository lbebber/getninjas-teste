const Backbone=require('backbone');
const FormPageView=require('./form-page');
const utils=require('../utils');
const _=require('lodash');

const FormView=Backbone.View.extend({
  tagName:'div',
  className:'Form',
  pagesElements:null,
  initialize(){
    this.listenTo(this.model,'change',this.change);
    this.render();
    this.showPage(this.model.get('currentPage'));
  },
  change(event){
    const changed=event.changed;

    if(!_.isUndefined(changed.currentPage)){
      this.showPage(changed.currentPage);
    }
  },
  showPage(id){
    this.pagesElements.forEach((page,i)=>{
      page.style.display=(i==id)?'block':'none';
    });
  },
  render(){
    const form=utils.createElementFromHTML(`
      <form class="Form-form">
      </form>
    `);
    this.el.appendChild(form);
    
    this.pagesElements=this.model.get('pages')
      .map((page,i,pages)=>{
        let container=utils.createElementFromHTML(`
          <fieldset class="FormPage" style="display:none"></fieldset>
        `);

        const last=i==pages.length-1;
        utils.appendHTMLToElement(
          (last?`
            <div class="FormPage-info">
              <h3>Falta pouco!</h3>
              <p>Preencha mais algumas informações para que os profissionais indicados possam entrar em contato.</p>
            </div>
          `:''),
          container
        );

        let pageView=new FormPageView({
          model:page,
        });
        container.appendChild(pageView.el);

        const buttonVerb=!last?'Continuar':'Buscar profissionais';
        const nextButton=utils.createElementFromHTML(
          `<button type="button" class="FormButton FormButton--confirm">${buttonVerb}</button>`
        );
        container.appendChild(nextButton);

        nextButton.addEventListener('click',event=>{
          this.model.proceed();
        });

        form.appendChild(container);
        return container;
      });

    return this;
  },
});

module.exports=FormView;
