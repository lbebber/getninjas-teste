const Backbone=require('backbone');
const InputView=require('./input');

const TextfieldView=InputView.extend({
  renderInput(){
    return `
      <input
        ${this.model.get('required')?'required':''}
        class="Textfield"
        type="${this.model.get('textType')}"
        name="${this.model.get('name')}"
        id="${this.model.get('name')}"
        placeholder="${this.model.get('placeholder')}"
      />
    `;
  }
});

module.exports=TextfieldView;


