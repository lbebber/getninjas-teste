const Backbone=require('backbone');
const utils=require('../utils');

const InputView=Backbone.View.extend({
  tagName:'li',
  className:'FormPage-item',
  initialize(){
    this.listenTo(this.model, 'change', this.change);
    this.render();
    this.changeValue();
  },
  events:{
    'change input,textarea':'changeValue',
  },
  changeValue(){
    this.model.set('value',this.el.querySelector('input,textarea,select').value);
    this.model.set('showRequiredWarning',false);
  },
  renderInput(){
    return '';
  },
  change(event){
    let changed=event.changed;
    let showRequiredWarning=changed.showRequiredWarning;
    if(!_.isUndefined(showRequiredWarning)){
      let requiredWarning=this.el.querySelector('.RequiredWarning');
      if(showRequiredWarning)
        requiredWarning.classList.add('RequiredWarning--visible');
      else
        requiredWarning.classList.remove('RequiredWarning--visible');
    }
  },
  render(){
    let showWarning=this.model.get('showRequiredWarning');

    this.el.innerHTML=`
      <label class="FormPage-itemLabel" for="${this.model.get('label')}">
        ${this.model.get('label')}
      </label>
      <div class="RequiredWarning ${this.model.get('showRequiredWarning')?'RequiredWarning--visible':''}">
        ${this.model.get('requiredWarningText')}
      </div>
      ${this.renderInput()}
    `;

    return this;
  },
});

module.exports=InputView;
