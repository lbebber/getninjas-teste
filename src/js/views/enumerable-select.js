const Backbone=require('backbone');
const EnumerableView=require('./enumerable');

const EnumerableSelectView=EnumerableView.extend({
  events:{
    'change select':'changeValue',
  },
  renderInput(){
    let placeholder=(this.model.get('placeholder')=='')?'':
      `<option disabled selected value=''>${this.model.get('placeholder')}</option>`
    ;

    let options=this.model.get('data')
      .map(item=>`
        <option value="${item}">${item}</option>
      `)
      .join('')
    ;

    let items=placeholder+options;
    
    return `
      <select 
        class="EnumerableSelect"
        ${this.model.get('required')?'required':''}
        name="${this.model.get('name')}"
        id="${this.model.get('name')}"
      >
        ${items}
      </select>
    `;
  }
});

module.exports=EnumerableSelectView;
