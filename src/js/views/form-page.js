const Backbone=require('backbone');
const Enumerable=require('../models/enumerable');
const EnumerableCheckboxView=require('./enumerable-checkbox');
const EnumerableSelectView=require('./enumerable-select');
const TextInput=require('../models/text-input');
const TextareaView=require('./textarea');
const TextfieldView=require('./textfield');

const FormPageView=Backbone.View.extend({
  tagName:'ol',
  className:'FormPage-container',
  initialize(){
    this.render();
  },
  render(){
    this.model.get('inputs')
      .map(model=>getViewFromModel(model))
      .forEach(view=>this.el.appendChild(view.el))
    ;
    return this;
  },
});

function getViewFromModel(model){
  let viewClass;
  if(model instanceof Enumerable){
    viewClass=model.get('multiple')?EnumerableCheckboxView:EnumerableSelectView;
  }else if(model instanceof TextInput){
    viewClass=model.get('textarea')?TextareaView:TextfieldView;
  }
  return new viewClass({model});
}

module.exports=FormPageView;
