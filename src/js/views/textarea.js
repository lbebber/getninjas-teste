const Backbone=require('backbone');
const InputView=require('./input');

const TextareaView=InputView.extend({
  renderInput(){
    return `
      <textarea
        ${this.model.get('required')?'required':''}
        class="Textarea Textfield"
        name="${this.model.get('name')}"
        id="${this.model.get('name')}"
        placeholder="${this.model.get('placeholder')}"
      ></textarea>
    `;
  }
});

module.exports=TextareaView;

