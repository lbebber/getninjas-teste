const _=require('lodash');

const createElementsFromHTML=html=>{
  let dummy=document.createElement('div');
  dummy.innerHTML=html;
  return dummy.childNodes;
}
const createElementFromHTML=html=>{
  const elements=_.toArray(createElementsFromHTML(html));
  for(let i=0;i<elements.length;i++){
    const element=elements[i];
    if(element.nodeType==1) return element;
  }
  return null;
}
const appendElementsToElement=(elements,node)=>
  _.toArray(elements).forEach(element=>node.appendChild(element))

const appendHTMLToElement=(html,node)=>
  appendElementsToElement(createElementsFromHTML(html),node)

module.exports={createElementsFromHTML,createElementFromHTML,appendElementsToElement,appendHTMLToElement};

