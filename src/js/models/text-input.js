const _=require('lodash');
const Input=require('./input');

const TextInput=Input.extend({
  defaults:_.assign(Input.prototype.defaults(),{
    textType:'text',
    textarea:false,
  }),
})

module.exports=TextInput;
