const Backbone=require('backbone');
const Pages=require('../collections/pages');
const parser=require('../parser');

const Form=Backbone.Model.extend({
  initialize(options){
    if(!(options.pages instanceof Pages)){
      this.set('pages',parser.getPagesFromData(options.pages));
    }

    const router=this.get('router');
    if(router!=null){
      let pages=this.get('pages');
      pages.forEach((page,i)=>{
        router.route(page.get('slug'),()=>this.goToPage(i));
      });
      router.navigate(this.getPageById(0).get('slug'));
    }
  },
  defaults:()=>({
    pages:new Pages([]),
    currentPage:0,
    router:null,
  }),
  proceed(){
    let currentPage=this.get('pages').at(this.get('currentPage'));
    let results=currentPage.validate();
    if(results.success){
      this.nextPage();
    }
  },
  navigateToPage(pageId){
    const router=this.get('router');
    if(router==null){
      this.goToPage(pageId);
    }else{
      const page=this.getPageById(pageId);
      router.navigate(page.get('slug'),{trigger:true});
    }
  },
  getPageById(id){
    return this.get('pages').at(id);
  },
  goToPage(pageId){
    this.set('currentPage',pageId);
  },
  nextPage(){
    let currentPage=this.get('currentPage');
    if(currentPage<this.get('pages').length-1)
      this.navigateToPage(currentPage+1);
    else
      this.submit();
  },
  validate(){
    return this.get('pages').map((page,i)=>page.validate());
  },
  submit(){
    let results=this.validate();
    if(results.every(result=>result.success)){
      alert('Fim');
    }else{
      alert('oops');
    }
  }
});

module.exports=Form;
