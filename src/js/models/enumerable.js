const _=require('lodash');
const Input=require('./input');

const Enumerable=Input.extend({
  defaults:_.assign(Input.prototype.defaults,{
    multiple:false,
    requiredWarningText:'Escolha uma opção',
    data:[],
  }),
});

module.exports=Enumerable;
