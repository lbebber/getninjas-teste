const Backbone=require('backbone');

const Input=Backbone.Model.extend({
  initialize(){
    this.on('change',this.teste);
  },
  defaults:()=>({
    type:null,
    required:false,
    showRequiredWarning:false,
    requiredWarningText:'Favor preencha o campo.',
    label:'',
    name:'',
    placeholder:'',
    type:'',
    value:'',
  }),
});

module.exports=Input;
