const Backbone=require('backbone');
const Inputs=require('../collections/inputs');

const FormPage=Backbone.Model.extend({
  initialize(options){
    // if(!(options.inputs instanceof Inputs))
    //   this.set('inputs',parser.getModelsFromData(options.inputs));
  },
  defaults:()=>({
    inputs:new Inputs(),
    title:'',
    slug:'',
  }),
  validate(){
    let inputs=this.get('inputs');
    inputs.forEach(input=>input.set('showRequiredWarning',false));
    let results=inputs
      .filter(input=>input.get('required'))
      .filter(input=>input.get('value')==[] || input.get('value')=='')
    ;
    if(results.length>0){
      results.forEach(result=>result.set('showRequiredWarning',true));
      return {success:false,results};
    }else return {success:true,results:[]};
  },
});

module.exports=FormPage;
